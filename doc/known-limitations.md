## Known limitations

Below are known limitations of the GitLab Operator.

### Certain components not supported

Below is a list of unsupported components:

- Praefect: [#136](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues/136)
